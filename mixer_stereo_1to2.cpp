/* Audio Library for Teensy 3.X
 * Copyright (c) 2014, Paul Stoffregen, paul@pjrc.com
 * 
 * Stereo one stream to two streams mixer
 * Copyright (c) 2017, NoisePorno
 *
 * Development of this audio library was funded by PJRC.COM, LLC by sales of
 * Teensy and Audio Adaptor boards.  Please support PJRC's efforts to develop
 * open source software by purchasing Teensy or other PJRC products.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice, development funding notice, and this permission
 * notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "mixer_stereo_1to2.h"
#include "utility/dspinst.h"

#ifdef LOG_GAIN
#include <RBD_SerialManager.h>
extern RBD::SerialManager 			serial_manager;
#endif


#if defined(KINETISK)
#define MULTI_UNITYGAIN 65536

static void applyGainInPlace(int16_t *data, int32_t mult)
{
	uint32_t *p = (uint32_t *)data;
	const uint32_t *end = (uint32_t *)(data + AUDIO_BLOCK_SAMPLES);

	if (mult != MULTI_UNITYGAIN)
	{
		do {
			uint32_t tmp32 = *p; // read 2 samples from *data
			int32_t val1 = signed_multiply_32x16b(mult, tmp32);
			int32_t val2 = signed_multiply_32x16t(mult, tmp32);
			val1 = signed_saturate_rshift(val1, 16, 0);
			val2 = signed_saturate_rshift(val2, 16, 0);
			*p++ = pack_16b_16b(val2, val1);
		} while (p < end);
	}
}

static void applyGainOutPlace(int16_t *data, const int16_t *in, int32_t mult)
{
	uint32_t *dst = (uint32_t *)data;
	const uint32_t *src = (uint32_t *)in;
	const uint32_t *end = (uint32_t *)(data + AUDIO_BLOCK_SAMPLES);
	
	if (mult == MULTI_UNITYGAIN) {
		do {
			*dst++ = *src++;
		} while (dst < end);
	} else {
		do {
			uint32_t tmp32 = *src++; // read 2 samples from *data
			int32_t val1 = signed_multiply_32x16b(mult, tmp32);
			int32_t val2 = signed_multiply_32x16t(mult, tmp32);
			val1 = signed_saturate_rshift(val1, 16, 0);
			val2 = signed_saturate_rshift(val2, 16, 0);
			*dst++ = pack_16b_16b(val2, val1);
		} while (dst < end);
	}
}

#elif defined(KINETISL)
#define MULTI_UNITYGAIN 256

static void applyGainInPlace(int16_t *data, int32_t mult)
{
	const int16_t *end = data + AUDIO_BLOCK_SAMPLES;

	if (mult != MULTI_UNITYGAIN)
	{
		do {
			int32_t val = *data * mult;
			*data++ = signed_saturate_rshift(val, 16, 0);
		} while (data < end);
	}
}

static void applyGainOutPlace(int16_t *dst, const int16_t *src, int32_t mult)
{
	const int16_t *end = dst + AUDIO_BLOCK_SAMPLES;

	if (mult == MULTI_UNITYGAIN) {
		do {
			*dst++ = *src++;
		} while (dst < end);
	} else {
		do {
			int32_t val = ((*src++ * mult) >> 8); // overflow possible??
			*dst++ = signed_saturate_rshift(val, 16, 0);
		} while (dst < end);
	}
}

#endif

void Mixer_Stereo_1to2::update(void)
{
	audio_block_t *out1=NULL, *out2=NULL;
	unsigned int channel_output;

	// compute left channel
	for (channel_output = 0; channel_output < 2; channel_output++)
	{
		out1 = receiveWritable(channel_output);
		if (out1)
		{
			// note that gain is only applied when needed by the two subfunctions

			out2 = allocate();
			if (out2)
			{
				// apply gain on input stream and store in second output stream 
				applyGainOutPlace(out2->data, out1->data, multiplier[2 + channel_output]);
			
				// give back the output block to the system
				transmit(out2, 2 + channel_output);
				release(out2);
			}
			// apply gain on input/output stream in place
			applyGainInPlace(out1->data, multiplier[channel_output]);
			
			// give back the output block to the system
			transmit(out1, channel_output);
			release(out1);
		}
	}
}


void Mixer_Stereo_1to2::print_gain(void)
{
#ifdef LOG_GAIN
	unsigned int channel_output;

	serial_manager.print("1to2 gain : ");
	for (channel_output = 0; channel_output < 4; channel_output++)
	{
		serial_manager.print(multiplier[channel_output]);
		serial_manager.print(" ");
	}
	serial_manager.println("");
#endif
}

