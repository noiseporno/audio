This block mixes two stereo input streams together into one single stereo output stream.

The first input stream is mapped on channels 0 and 1.
The second input stream is mapped on channel 2 and 3.
The output stream is mapped on channels 0 and 1.

The gain() function channel maps on the input stream numbering.
The gain is applied on the input stream before addition:
  Out[0] = Gain[0] * In[0] + Gain[2] * In[2]
  Out[1] = Gain[1] * In[1] + Gain[3] * In[3]