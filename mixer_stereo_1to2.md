This block expands one stereo input stream into two stereo output streams with individual gains.

The input stream is mapped on channels 0 and 1.
The first output stream is mapped on channels 0 and 1.
The second output stream is mapped on channel 2 and 3.

The gain() function channel maps on the input stream numbering.
The gain is applied on the input stream before addition:
  Out[0] = Gain[0] * In[0]
  Out[1] = Gain[1] * In[1]
  Out[2] = Gain[2] * In[0]
  Out[3] = Gain[3] * In[1]